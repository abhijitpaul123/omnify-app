//
//  Singleton.m
//  OmnifyAppProject
//
//  Created by Abhijit Paul on 28/05/18.
//  Copyright © 2018 Abhijit Paul. All rights reserved.
//

#import "Singleton.h"

@implementation Singleton

+ (UIColor *)front_end_color {
    return [UIColor colorWithRed:0.92 green:0.37 blue:0.20 alpha:1.0];
}

+ (UIView *) radiusOfView: (UIView *)view withBorderColor: (UIColor *)borderColor andShadow: (BOOL)shadow{
    // border radius
    [view.layer setCornerRadius:5.0f];
    
    if (shadow) {
        // drop shadow
        [view.layer setShadowColor:[UIColor lightGrayColor].CGColor];
        [view.layer setShadowOpacity:0.8];
        [view.layer setShadowRadius:5.0];
        [view.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    }
    
    view.layer.borderColor = borderColor.CGColor;
    view.layer.borderWidth = 2.0f;
    
    return view;
}

+ (NSDate *) dateAndTimeForTimeStamp:(NSString *)timestampString {
    double timeStamp = [timestampString doubleValue];
    NSTimeInterval timeInterval=timeStamp/1;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    return date;
}

+ (NSString *)timeAgoStringFromDate:(NSDate *)date {
    NSDateComponentsFormatter *formatter = [[NSDateComponentsFormatter alloc] init];
    formatter.unitsStyle = NSDateComponentsFormatterUnitsStyleFull;
    
    NSDate *now = [NSDate date];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitWeekOfMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond)
                                               fromDate:date
                                                 toDate:now
                                                options:0];
    
    if (components.year > 0) {
        formatter.allowedUnits = NSCalendarUnitYear;
    } else if (components.month > 0) {
        formatter.allowedUnits = NSCalendarUnitMonth;
    } else if (components.weekOfMonth > 0) {
        formatter.allowedUnits = NSCalendarUnitWeekOfMonth;
    } else if (components.day > 0) {
        formatter.allowedUnits = NSCalendarUnitDay;
    } else if (components.hour > 0) {
        formatter.allowedUnits = NSCalendarUnitHour;
    } else if (components.minute > 0) {
        formatter.allowedUnits = NSCalendarUnitMinute;
    } else {
        formatter.allowedUnits = NSCalendarUnitSecond;
    }
    
    NSString *formatString = NSLocalizedString(@"%@ ago", @"Used to say how much time has passed. e.g. '2 hours ago'");
    
    return [NSString stringWithFormat:formatString, [formatter stringFromDateComponents:components]];
}




@end
