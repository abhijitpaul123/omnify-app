//
//  Singleton.h
//  OmnifyAppProject
//
//  Created by Abhijit Paul on 28/05/18.
//  Copyright © 2018 Abhijit Paul. All rights reserved.
//

#import "AppDelegate.h"

@interface Singleton : NSObject{
    
}

+(UIColor *) front_end_color;

+ (UIView *) radiusOfView: (UIView *)view withBorderColor: (UIColor *)borderColor andShadow: (BOOL)shadow;

+ (NSDate *) dateAndTimeForTimeStamp:(NSString *)timestampString;

+ (NSString *)timeAgoStringFromDate:(NSDate *)date;

@end
