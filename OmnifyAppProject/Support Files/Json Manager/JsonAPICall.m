//
//  JsonAPICall.m
//  OmnifyAppProject
//
//  Created by Abhijit Paul on 28/05/18.
//  Copyright © 2018 Abhijit Paul. All rights reserved.
//

#import "JsonAPICall.h"

#define kRequestTimeOut 30.00

@implementation JsonAPICall

- (void)getTopStories{
    
    NSString *stringUrl = [NSString stringWithFormat:@"https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty"];
    
    [JSONManeger JSONRequestWithparameters:nil Request:stringUrl success:^(NSString *request, id JSON) {
        if ([self.delegate respondsToSelector:@selector(didSucceedRequest: jsonData:)]){
            [self.delegate didSucceedRequest:request jsonData:JSON];
        }
    } failure:^(NSString *request, NSError *error) {
        if ([self.delegate respondsToSelector:@selector(didFailedRequest: withError:)]){
            [self.delegate didFailedRequest:request withError:error];
        }
    } typeOfMethod:@"GET"];
}

- (void)getStoryDescriptionForID: (NSString *)idNumber {
    
    NSString *stringUrl = [NSString stringWithFormat:@"https://hacker-news.firebaseio.com/v0/item/%@.json?print=pretty",idNumber];
    
    [JSONManeger JSONRequestWithparameters:nil Request:stringUrl success:^(NSString *request, id JSON) {
        if ([self.delegate respondsToSelector:@selector(didSucceedRequest: jsonData:)]){
            [self.delegate didSucceedRequest:request jsonData:JSON];
        }
    } failure:^(NSString *request, NSError *error) {
        if ([self.delegate respondsToSelector:@selector(didFailedRequest: withError:)]){
            [self.delegate didFailedRequest:request withError:error];
        }
    } typeOfMethod:@"GET"];
}

- (void)getCommentForArticleWithID: (NSString *)idNumber{
    
    NSString *stringUrl = [NSString stringWithFormat:@"https://hacker-news.firebaseio.com/v0/item/%@.json?print=pretty", idNumber];
    
    [JSONManeger JSONRequestWithparameters:nil Request:stringUrl success:^(NSString *request, id JSON) {
        if ([self.delegate respondsToSelector:@selector(didSucceedRequest: jsonData:)]){
            [self.delegate didSucceedRequest:request jsonData:JSON];
        }
    } failure:^(NSString *request, NSError *error) {
        if ([self.delegate respondsToSelector:@selector(didFailedRequest: withError:)]){
            [self.delegate didFailedRequest:request withError:error];
        }
    } typeOfMethod:@"GET"];
}


@end
