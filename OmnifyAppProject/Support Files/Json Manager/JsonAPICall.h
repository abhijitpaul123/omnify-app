//
//  JsonAPICall.h
//  OmnifyAppProject
//
//  Created by Abhijit Paul on 28/05/18.
//  Copyright © 2018 Abhijit Paul. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "JSONManeger.h"
#import "Singleton.h"
#import "AFNetworking.h"

@protocol JSONDelegate <NSObject>

//success
-(void)didSucceedRequest:(NSString *)request jsonData:(id)JSON;

//failure
-(void)didFailedRequest:(NSString *)request withError:(NSError *)error;

@end

@interface JsonAPICall : NSObject
@property (nonatomic, weak) id <JSONDelegate> delegate;


#pragma mark - Hacker News Top Stories API
- (void) getTopStories;

#pragma mark - Hacker News Top Stories Description API
- (void)getStoryDescriptionForID: (NSString *)idNumber;

#pragma mark - Hacker News Get Comments API
- (void)getCommentForArticleWithID: (NSString *)idNumber;



@end
