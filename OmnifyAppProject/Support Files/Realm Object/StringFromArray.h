//
//  StringFromArray.h
//  OmnifyAppProject
//
//  Created by Abhijit Paul on 31/05/18.
//  Copyright © 2018 Abhijit Paul. All rights reserved.
//

#import <Realm/Realm.h>

@interface StringFromArray : RLMObject
@property NSString *stringValue;
@end
//RLM_ARRAY_TYPE(StringFromArray)
