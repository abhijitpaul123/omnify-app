//
//  TopStoriesObject.h
//  OmnifyAppProject
//
//  Created by Abhijit Paul on 31/05/18.
//  Copyright © 2018 Abhijit Paul. All rights reserved.
//

#import <Realm/Realm.h>
#import "StringFromArray.h"

//@class StringFromArray;

RLM_ARRAY_TYPE(StringFromArray)

@interface TopStoriesObject : RLMObject
@property NSString *idNumber;
@property BOOL deleted;
@property NSString *type;
@property NSString *by;
@property NSString *time;
@property NSString *text;
@property BOOL dead;
@property NSString *parent;
@property NSString *poll;
@property RLMArray<StringFromArray>* kids;

@property NSString *url;
@property NSString *score;
@property NSString *title;
@property RLMArray<StringFromArray> *parts;
@property NSString *descendants;

@end


