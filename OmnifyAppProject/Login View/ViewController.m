//
//  ViewController.m
//  OmnifyAppProject
//
//  Created by Abhijit Paul on 28/05/18.
//  Copyright © 2018 Abhijit Paul. All rights reserved.
//

#import "ViewController.h"
#import "ArticleNavigationViewController.h"

@interface ViewController () {
    GIDGoogleUser* savedGoogleUser;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;
    [[GIDSignIn sharedInstance] signIn];

    self.view.backgroundColor = [Singleton front_end_color];
    [Singleton radiusOfView:self.google_bg_view withBorderColor:[UIColor clearColor] andShadow:NO];
    [self.google_sign_in_button addTarget:self action:@selector(google_sign_in_button_action:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewWillAppear:(BOOL)animated {
    self.handle = [[FIRAuth auth]
                   addAuthStateDidChangeListener:^(FIRAuth *_Nonnull auth, FIRUser *_Nullable user) {
                       // ...
                   }];
}

- (IBAction)google_sign_in_button_action:(id)sender {
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;
    [[GIDSignIn sharedInstance] signIn];
}

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
//    [myActivityIndicator stopAnimating];
}

- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    if (error == nil) {
        GIDAuthentication *authentication = user.authentication;
        FIRAuthCredential *credential =
        [FIRGoogleAuthProvider credentialWithIDToken:authentication.idToken
                                         accessToken:authentication.accessToken];
        [[FIRAuth auth] signInAndRetrieveDataWithCredential:credential
                                                 completion:^(FIRAuthDataResult * _Nullable authResult,
                                                              NSError * _Nullable error) {
                                                     if (error) {
                                                         return;
                                                     }
                                                     // User successfully signed in...
                                                     FIRUser *user = [FIRAuth auth].currentUser;
                                                     if (user) {
                                                         // The user's ID, unique to the Firebase project.
                                                         // Do NOT use this value to authenticate with your backend server,
                                                         // if you have one. Use getTokenWithCompletion:completion: instead.
                                                         [self successfullySignedIn:user];
                                                     }
                                                 }];
    }
}

- (void) successfullySignedIn:(FIRUser*)user {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ArticleNavigationViewController *articleNavigationVC = [storyboard instantiateViewControllerWithIdentifier:@"ArticleNavigationViewController"];
    [self presentViewController:articleNavigationVC animated:YES completion:NULL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
