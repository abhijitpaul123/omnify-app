//
//  ViewController.h
//  OmnifyAppProject
//
//  Created by Abhijit Paul on 28/05/18.
//  Copyright © 2018 Abhijit Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
@import GoogleSignIn;
@import Firebase;

@interface ViewController : UIViewController<GIDSignInUIDelegate, GIDSignInDelegate>
@property(strong, nonatomic) FIRAuthStateDidChangeListenerHandle handle;
@property (weak, nonatomic) IBOutlet UIView *google_bg_view;
@property (weak, nonatomic) IBOutlet UIImageView *google_imageView;
@property (weak, nonatomic) IBOutlet UILabel *google_label;
@property (weak, nonatomic) IBOutlet UIButton *google_sign_in_button;



@end

