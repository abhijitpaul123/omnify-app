//
//  StoryTableViewCell.h
//  OmnifyAppProject
//
//  Created by Abhijit Paul on 30/05/18.
//  Copyright © 2018 Abhijit Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *cell_bg_view;
@property (weak, nonatomic) IBOutlet UILabel *article_title;
@property (weak, nonatomic) IBOutlet UILabel *submitter_label;
@property (weak, nonatomic) IBOutlet UILabel *time_label;
@property (weak, nonatomic) IBOutlet UILabel *comment_count_label;
@property (weak, nonatomic) IBOutlet UILabel *score_label;
@end
