//
//  StoryModel.h
//  OmnifyAppProject
//
//  Created by Abhijit Paul on 30/05/18.
//  Copyright © 2018 Abhijit Paul. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoryModel : NSObject
@property (strong, nonatomic) NSString *by;
@property (strong, nonatomic) NSString *descendants;
@property (strong, nonatomic) NSString *idNumber;
@property (strong, nonatomic) NSArray *kids;
@property (strong, nonatomic) NSString *score;
@property (strong, nonatomic) NSString *time;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *url;

+ (StoryModel *)createEmptyObject;
@end
