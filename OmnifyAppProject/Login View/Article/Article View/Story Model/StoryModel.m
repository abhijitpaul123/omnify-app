//
//  StoryModel.m
//  OmnifyAppProject
//
//  Created by Abhijit Paul on 30/05/18.
//  Copyright © 2018 Abhijit Paul. All rights reserved.
//

#import "StoryModel.h"

@implementation StoryModel
@synthesize by;
@synthesize descendants;
@synthesize idNumber;
@synthesize kids;
@synthesize score;
@synthesize time;
@synthesize title;
@synthesize type;
@synthesize url;

+ (StoryModel *)createEmptyObject{
    StoryModel *model = [[StoryModel alloc] init];
    return model;
}
@end
