//
//  ArticleViewController.m
//  OmnifyAppProject
//
//  Created by Abhijit Paul on 29/05/18.
//  Copyright © 2018 Abhijit Paul. All rights reserved.
//

#import "ArticleViewController.h"
#import "StoryTableViewCell.h"
#import "JsonAPICall.h"
#import "StoryModel.h"
#import "ArticleDetailViewController.h"
#import <Realm/Realm.h>
#import "TopStoriesObject.h"

@interface ArticleViewController () <JSONDelegate> {
    RLMResults *savedData;
    TopStoriesObject *selectedDataObject;
}

@end

@implementation ArticleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.numberOfStoriesToFetch = 50;
    [self setupNavigationBar];
    self.storyDescriptionsArray = [[NSMutableArray alloc] init];
    [self callApiToGetStoryID];
}

- (void)setupNavigationBar {
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, self.view.frame.size.width-60, self.navigationController.navigationBar.frame.size.height)];
    titleLabel.text = @"Top Stories";
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel setAdjustsFontSizeToFitWidth:YES];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:17]];
    self.navigationItem.titleView = titleLabel;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [Singleton front_end_color];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];

    UIBarButtonItem *logout_btn = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Logout"
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(logout_btn_action:)];
    self.navigationItem.rightBarButtonItem = logout_btn;
}

-(IBAction)logout_btn_action:(id)sender
{
    NSError *signOutError;
    BOOL status = [[FIRAuth auth] signOut:&signOutError];
    if (!status) {
        NSLog(@"Error signing out: %@", signOutError);
        return;
    }
    
    [[GIDSignIn sharedInstance] signOut];
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void) callApiToGetStoryID {
    [self.activityIndicator setHidden:NO];
    [self.activityIndicator startAnimating];
    JsonAPICall *apicall = [[JsonAPICall alloc] init];
    apicall.delegate = self;
    [apicall getTopStories];
}

- (void)didFailedRequest:(NSString *)request withError:(NSError *)error {
    NSLog(@"request: %@", request);
    NSLog(@"error: %@", error);
}

- (void)didSucceedRequest:(NSString *)request jsonData:(id)JSON {
    
    if ([[request lastPathComponent] isEqualToString:@"topstories.json?print=pretty"]) {
        self.topStoriesIds = [[NSMutableArray alloc] initWithArray:JSON];
        [self fetchStoryDescriptionUpto:self.numberOfStoriesToFetch];
    } else {
        StoryModel *storyModel = [StoryModel createEmptyObject];
        storyModel.by = [NSString stringWithFormat:@"%@",[JSON valueForKey:@"by"]];
        storyModel.descendants = [NSString stringWithFormat:@"%@",[JSON valueForKey:@"descendants"]];
        storyModel.idNumber = [NSString stringWithFormat:@"%@",[JSON valueForKey:@"id"]];
        storyModel.kids = [JSON valueForKey:@"kids"];
        storyModel.score = [NSString stringWithFormat:@"%@",[JSON valueForKey:@"score"]];
        storyModel.time = [NSString stringWithFormat:@"%@",[JSON valueForKey:@"time"]];
        storyModel.title = [NSString stringWithFormat:@"%@",[JSON valueForKey:@"title"]];
        storyModel.type = [NSString stringWithFormat:@"%@",[JSON valueForKey:@"type"]];
        storyModel.url = [NSString stringWithFormat:@"%@",[JSON valueForKey:@"url"]];
                
        RLMResults <TopStoriesObject *> *topStoryArray = [TopStoriesObject objectsWhere:[NSString stringWithFormat:@"idNumber = '%@'", storyModel.idNumber]];
        
        if (topStoryArray.count == 0) {
            NSLog(@"Object not found. Insert");
            [self insertDataIntoDataBaseWithModel:storyModel];
        } else {
            NSLog(@"Object found. Don't Insert");
        }
        
        
        [self.storyDescriptionsArray addObject:storyModel];
        if (self.storyDescriptionsArray.count == self.numberOfStoriesToFetch) {
            [self.activityIndicator stopAnimating];
            [self.activityIndicator setHidden:YES];
            [self.article_list_tableView reloadData];
        }
    }
}

- (void) insertDataIntoDataBaseWithModel: (StoryModel *)model {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    
    TopStoriesObject *information = [[TopStoriesObject alloc] init];
    information.by = model.by;
    information.descendants = model.descendants;
    information.idNumber = model.idNumber;
    for (NSString *value in model.kids) {
        StringFromArray *string = [StringFromArray new];
        string.stringValue = [NSString stringWithFormat:@"%@", value];
        [information.kids addObject:string];
    }
    information.score = model.score;
    information.time = model.time;
    information.title = model.title;
    information.type = model.type;
    information.url = model.url;
    
    [realm addObject:information];
    [realm commitWriteTransaction];
}

- (void) fetchStoryDescriptionUpto: (int)count {
    for (int i = 0 ; i < count ; i++) {
        
        RLMResults <TopStoriesObject *> *topStoryArray = [TopStoriesObject objectsWhere:[NSString stringWithFormat:@"idNumber = '%@'", [self.topStoriesIds objectAtIndex:i]]];
        
        if (topStoryArray.count == 0) {
            NSLog(@"Object not found. Call");
            JsonAPICall *apicall = [[JsonAPICall alloc] init];
            apicall.delegate = self;
            [apicall getStoryDescriptionForID:[self.topStoriesIds objectAtIndex:i]];
        } else {
            NSLog(@"Object found. Don't Call");
            StoryModel *model = [StoryModel createEmptyObject];
            TopStoriesObject *object = [topStoryArray objectAtIndex:0];
            
            model.by = object.by;
            model.descendants = object.descendants;
            model.idNumber = object.idNumber;
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            for (StringFromArray *element in object.kids) {
                [array addObject:[element valueForKey:@"stringValue"]];
            }
            model.kids = array;
            model.score = object.score;
            model.time = object.time;
            model.title = object.title;
            model.type = object.type;
            model.url = object.url;

            [self.storyDescriptionsArray addObject:model];
            if (self.storyDescriptionsArray.count == self.numberOfStoriesToFetch) {
                [self.activityIndicator stopAnimating];
                [self.activityIndicator setHidden:YES];
                [self.article_list_tableView reloadData];
            }
        }
        
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.storyDescriptionsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"StoryTableViewCell";
    StoryTableViewCell *cell = (StoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    StoryModel *storyModel = [StoryModel createEmptyObject];
    storyModel = [self.storyDescriptionsArray objectAtIndex:indexPath.row];
    
    cell.score_label.text = storyModel.score;
    cell.article_title.text = storyModel.title;
    cell.submitter_label.text = storyModel.by;
    cell.time_label.text = [Singleton timeAgoStringFromDate:[Singleton dateAndTimeForTimeStamp:storyModel.time]];
    cell.comment_count_label.text = storyModel.descendants;
    cell.contentView.backgroundColor = [UIColor whiteColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    StoryModel *storyModel = [StoryModel createEmptyObject];
    storyModel = [self.storyDescriptionsArray objectAtIndex:indexPath.row];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ArticleDetailViewController *articleDetailVC = [storyboard instantiateViewControllerWithIdentifier:@"ArticleDetailViewController"];
    articleDetailVC.storyModel = storyModel;
    [self.navigationController pushViewController:articleDetailVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
