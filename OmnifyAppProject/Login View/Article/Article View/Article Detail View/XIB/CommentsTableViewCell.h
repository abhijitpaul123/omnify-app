//
//  CommentsTableViewCell.h
//  OmnifyAppProject
//
//  Created by Abhijit Paul on 31/05/18.
//  Copyright © 2018 Abhijit Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *cell_bg_view;
@property (weak, nonatomic) IBOutlet UILabel *date_label;
@property (weak, nonatomic) IBOutlet UILabel *comment_label;

@end
