//
//  ArticleDetailViewController.m
//  OmnifyAppProject
//
//  Created by Abhijit Paul on 30/05/18.
//  Copyright © 2018 Abhijit Paul. All rights reserved.
//

#import "ArticleDetailViewController.h"
#import "JsonAPICall.h"
#import "CommentsTableViewCell.h"
#import "CommentModel.h"

@interface ArticleDetailViewController () <JSONDelegate> {
    NSMutableArray *commentsArray;
}
@end

@implementation ArticleDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    if (self.storyModel.url.length == 0 || [self.storyModel.url isEqualToString:@"(null)"]) {
        self.article_web_view.hidden = YES;
        self.comment_tableView.hidden = NO;
        self.comment_article_segmentedControl.hidden = YES;
        self.comments_label.hidden = NO;
    } else {
        [self.comment_article_segmentedControl addTarget:self action:@selector(segmentedControlValueChanged:) forControlEvents:UIControlEventValueChanged];
        self.article_web_view.hidden = YES;
        self.comment_tableView.hidden = NO;
        self.comment_article_segmentedControl.hidden = NO;
        self.comments_label.hidden = YES;
        [self setupWebView];
    }
    commentsArray = [[NSMutableArray alloc] init];
    [self getComments];
}

- (void) setupWebView {
    NSURL *nsurl=[NSURL URLWithString:self.storyModel.url];
    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
    [self.article_web_view loadRequest:nsrequest];

}

- (IBAction)segmentedControlValueChanged:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == 0) {
        self.comment_tableView.hidden = NO;
        self.article_web_view.hidden = YES;
    } else {
        self.article_web_view.hidden = NO;
        self.comment_tableView.hidden = YES;
    }
}

- (void) setupUI {
    self.top_bg_view.backgroundColor = [Singleton front_end_color];
    self.title_label.text = self.storyModel.title;
    self.by_label.text = self.storyModel.by;
    self.date_label.text = [Singleton timeAgoStringFromDate:[Singleton dateAndTimeForTimeStamp:self.storyModel.time]];
    self.comment_article_segmentedControl.backgroundColor = [Singleton front_end_color];
}

- (void) getComments {
    for (NSString *idNumber in self.storyModel.kids) {
        
//        [self getCommentsOfArticleForID:idNumber];
        
        RLMResults <TopStoriesObject *> *topCommentsArray = [TopStoriesObject objectsWhere:[NSString stringWithFormat:@"idNumber = '%@'", idNumber]];
        
        if (topCommentsArray.count == 0) {
            NSLog(@"Object not found. Call");
            [self getCommentsOfArticleForID:idNumber];
        } else {
            NSLog(@"Object found. Don't Call");
            CommentModel *model = [CommentModel createEmptyObject];
            TopStoriesObject *object = [topCommentsArray objectAtIndex:0];
            
            model.by = object.by;
            model.idNumber = object.idNumber;
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            for (StringFromArray *element in object.kids) {
                [array addObject:[element valueForKey:@"stringValue"]];
            }
            model.kids = array;
            model.parent = object.parent;
            model.text = object.text;
            model.time = object.time;
            model.type = object.type;
            
            [commentsArray addObject:model];
            if (commentsArray.count == self.storyModel.kids.count) {
                [self.comment_tableView reloadData];
            }
        }
    }
}

- (void) getCommentsOfArticleForID: (NSString *)idNumber {
    JsonAPICall *apicall = [[JsonAPICall alloc] init];
    apicall.delegate = self;
    [apicall getCommentForArticleWithID:idNumber];
}

- (void)didFailedRequest:(NSString *)request withError:(NSError *)error {
    NSLog(@"request: %@", request);
    NSLog(@"error: %@", error);
}

- (void)didSucceedRequest:(NSString *)request jsonData:(id)JSON {
    NSLog(@"request: %@", request);
    NSLog(@"data: %@", JSON);
    
    CommentModel *commentModel = [CommentModel createEmptyObject];
    commentModel.by = [NSString stringWithFormat:@"%@",[JSON valueForKey:@"by"]];
    commentModel.idNumber = [NSString stringWithFormat:@"%@",[JSON valueForKey:@"id"]];
    commentModel.kids = [JSON valueForKey:@"kids"];
    commentModel.parent = [NSString stringWithFormat:@"%@",[JSON valueForKey:@"parent"]];
    commentModel.text = [NSString stringWithFormat:@"%@",[JSON valueForKey:@"text"]];
    commentModel.time = [NSString stringWithFormat:@"%@",[JSON valueForKey:@"time"]];
    commentModel.type = [NSString stringWithFormat:@"%@",[JSON valueForKey:@"type"]];
    
    RLMResults <TopStoriesObject *> *topCommentsArray = [TopStoriesObject objectsWhere:[NSString stringWithFormat:@"idNumber = '%@'", commentModel.idNumber]];
    
    if (topCommentsArray.count == 0) {
        NSLog(@"Object not found. Insert");
        [self insertDataIntoDataBaseWithModel:commentModel];
    } else {
        NSLog(@"Object found. Don't Insert");
    }


    [commentsArray addObject:commentModel];
    
    if (commentsArray.count == self.storyModel.kids.count) {
        [self.comment_tableView reloadData];
    }
}

- (void)insertDataIntoDataBaseWithModel: (CommentModel *)model {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    
    TopStoriesObject *information = [[TopStoriesObject alloc] init];
    information.by = model.by;
    information.idNumber = model.idNumber;
    for (NSString *value in model.kids) {
        StringFromArray *string = [StringFromArray new];
        string.stringValue = [NSString stringWithFormat:@"%@", value];
        [information.kids addObject:string];
    }
    information.parent = model.parent;
    information.text = model.text;
    information.time = model.time;
    information.type = model.type;
    
    [realm addObject:information];
    [realm commitWriteTransaction];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return commentsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CommentModel *commentModel = [commentsArray objectAtIndex:indexPath.row];

    NSString *cellText = commentModel.text;
    CGSize constraintSize = CGSizeMake(self.view.bounds.size.width, MAXFLOAT);
    
    CGRect labelSize = [cellText boundingRectWithSize:constraintSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]} context:nil];
    return labelSize.size.height+60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CommentsTableViewCell";
    CommentsTableViewCell *cell = (CommentsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    CommentModel *commentModel = [commentsArray objectAtIndex:indexPath.row];
    
    cell.date_label.text = [Singleton timeAgoStringFromDate:[Singleton dateAndTimeForTimeStamp:commentModel.time]];
    
    if ([commentModel.text isEqualToString:@"(null)"]) {
        cell.comment_label.text = @"";
    } else {
        cell.comment_label.text = commentModel.text;
    }

    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
