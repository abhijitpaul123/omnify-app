//
//  ArticleDetailViewController.h
//  OmnifyAppProject
//
//  Created by Abhijit Paul on 30/05/18.
//  Copyright © 2018 Abhijit Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StoryModel.h"
#import <WebKit/WebKit.h>
#import <Realm/Realm.h>
#import "TopStoriesObject.h"


@interface ArticleDetailViewController : UIViewController

@property (nonatomic, strong) StoryModel *storyModel;
@property (weak, nonatomic) IBOutlet UIView *top_bg_view;
@property (weak, nonatomic) IBOutlet UILabel *title_label;
@property (weak, nonatomic) IBOutlet UILabel *by_label;
@property (weak, nonatomic) IBOutlet UILabel *date_label;
@property (weak, nonatomic) IBOutlet UISegmentedControl *comment_article_segmentedControl;
@property (weak, nonatomic) IBOutlet UIView *bottom_bg_view;
@property (weak, nonatomic) IBOutlet WKWebView *article_web_view;
@property (weak, nonatomic) IBOutlet UITableView *comment_tableView;
@property (weak, nonatomic) IBOutlet UILabel *comments_label;

@end
