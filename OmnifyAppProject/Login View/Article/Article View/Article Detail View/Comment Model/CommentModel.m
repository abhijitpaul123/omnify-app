//
//  CommentModel.m
//  OmnifyAppProject
//
//  Created by Abhijit Paul on 31/05/18.
//  Copyright © 2018 Abhijit Paul. All rights reserved.
//

#import "CommentModel.h"

@implementation CommentModel
@synthesize by;
@synthesize idNumber;
@synthesize kids;
@synthesize parent;
@synthesize text;
@synthesize time;
@synthesize type;

+ (CommentModel *)createEmptyObject{
    CommentModel *model = [[CommentModel alloc] init];
    return model;
}

@end
