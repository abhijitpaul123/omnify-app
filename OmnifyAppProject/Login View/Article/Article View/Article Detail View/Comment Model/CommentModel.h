//
//  CommentModel.h
//  OmnifyAppProject
//
//  Created by Abhijit Paul on 31/05/18.
//  Copyright © 2018 Abhijit Paul. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommentModel : NSObject
@property (strong, nonatomic) NSString *by;
@property (strong, nonatomic) NSString *idNumber;
@property (strong, nonatomic) NSArray *kids;
@property (strong, nonatomic) NSString *parent;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSString *time;
@property (strong, nonatomic) NSString *type;

+ (CommentModel *)createEmptyObject;
@end
