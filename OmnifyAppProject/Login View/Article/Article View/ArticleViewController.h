//
//  ArticleViewController.h
//  OmnifyAppProject
//
//  Created by Abhijit Paul on 29/05/18.
//  Copyright © 2018 Abhijit Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *article_list_tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property NSArray *topStoriesIds;
@property NSMutableArray *storyDescriptionsArray;
@property int numberOfStoriesToFetch;


@end
