//
//  ArticleNavigationViewController.m
//  OmnifyAppProject
//
//  Created by Abhijit Paul on 29/05/18.
//  Copyright © 2018 Abhijit Paul. All rights reserved.
//

#import "ArticleNavigationViewController.h"

@interface ArticleNavigationViewController ()

@end

@implementation ArticleNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
