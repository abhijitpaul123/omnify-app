# Omnify App Project

A Hacker New reader iOS Application written in Objective C.

## Features

View "Top Stories".
Read full article in web view and top comments.
Uses the official Firebase based Hacker News API

## Supported Devices

iPhone with iOS 10 or later

## Installing

Go to application folder from terminal
pod install (requires CocoaPods)
open OmnifyAppProject.xcworkspace

## Architecture

1. View Controllers
        ViewController --> Shows login view 
        ArticleViewController --> Shows list of articles
        ArticleDetailViewController --> Shows the top comments and full article in web view.
        
2. XIB
        StoryTableViewCell --> shows the story element in top story list
        CommentsTableVIewCell --> shows the comment element in top comments list
        
3. Models
        StoryModel --> holds the values for top story api call
        CommentModel --> holds the values for comments api call
        
4. Navigation Controller
        ArticleNavigationViewController --> used to manage stack of ArticleViewController and ArticleDetailViewController

## Support Files

1. Realm
        TopStoriesObject --> stores data fetched from top stories api and comment api as both has almost same key value pair.
        StringFromArray --> used to store array elements from "kids" key in stories api and comment api. Extention to RLMArray.
        
2. Json Manager
        JsonAPICall --> Acts as a mediatior to pass network api call request to JSONManager and send back the response to perticular delegate.
        JSONManager --> Makes network connection to the backend and fetches the response.
        
3. Singleton
        Singleton --> common methods which can be accessed by any class.
        
## Pods

GoogleSignIn --> used to authenticate application using google
Firebase/Core --> Firebase prerequisite libraries and analytics
Firebase/Auth --> Authentication of the user during sign in
Firebase/Database --> Connecting to Hacker News Database
AFNetworking --> Handle network connection with the database
Realm --> Save fetched data in application so that next time the same data will not have to be fetched through api call.

## Main Functional Description

Application can be signed in using google sign in.
        - (IBAction)google_sign_in_button_action:(id)sender;
        
Firebase is used to authenticate google sign in.
        - (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error;
        
Once the Firebase authentication is success the user is navigated to ArticleViewController
        - (void) successfullySignedIn:(FIRUser*)user;
        
In ArticleViewController the api to fetch id's for Top Stories is called
        - (void) callApiToGetStoryID;

Once the response is fetched we call the api to fetch Story Description  upto 50 id's "numberOfStoriesToFetch" for the id whose data is not present in the Realm Database
        - (void) fetchStoryDescriptionUpto: (int)count;
        
If the id is present in the Realm Database then we do not call the Story Description API for that id and load it in the list.
        RLMResults <TopStoriesObject *> *topStoryArray = [TopStoriesObject objectsWhere:[NSString stringWithFormat:@"idNumber = '%@'", [self.topStoriesIds objectAtIndex:i]]];
        if (topStoryArray.count == 0) {
            //object not found. Call Story Description API
        } else {
            //object found. Fetch object from Realm Database and load it to table cell data array.
            }

Once the Story Description data is fetched for the id's not present in the Realm Database the data is loaded to table cell data array "storyDescriptionsArray" and the data is also added to Realm Database
        - (void) insertDataIntoDataBaseWithModel: (StoryModel *)model;
        
The table view is loaded once we get all the 50 Top Stories to show
        [self.article_list_tableView reloadData];
        
Once the user clicks on a perticular story the ArticleDetailViewController is loaded
        - (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;

If the URL to story article is present the segmentedControl is shown which can switch between comments and article web view. If the URL is not present only comments section is shown.
        if (self.storyModel.url.length == 0 || [self.storyModel.url isEqualToString:@"(null)"]) {
            //show only comments
        } else {
            //show comments and article web view.
        }
        
Fetch comments data which is not present in the Realm Database
        - (void) getComments;
        
If the id is present in the Realm Database then we do not call the Comments API for that id and load it in the list.
        RLMResults <TopStoriesObject *> *topCommentsArray = [TopStoriesObject objectsWhere:[NSString stringWithFormat:@"idNumber = '%@'", [self.topStoriesIds objectAtIndex:i]]];
        if (topCommentsArray.count == 0) {
            //object not found. Call Comments API
        } else {
            //object found. Fetch object from Realm Database and load it to table cell data array.
        }

Once the Comments data is fetched for the id's not present in the Realm Database the data is loaded to table cell data array "commentsArray" and the data is also added to Realm Database
        - (void) insertDataIntoDataBaseWithModel: (StoryModel *)model;
        
The table view is loaded once we get all the top comments to show
        [self.comment_tableView reloadData];
        
User can switch between comments to read the top comments and article to read the article which is loaded in the web view
        - (IBAction)segmentedControlValueChanged:(UISegmentedControl *)sender;
        
The cell height for comments table is dynamic and depends on the comment length.
        - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;

